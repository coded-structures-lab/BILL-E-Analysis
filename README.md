# BILL-E Analysis

Math for BILL-E

## Who is BILL-E?

BILL-E is a robot invented by Ben Jenett. 

## What questions do we want to answer?
 1. Why are the robot dimensions what they are? Are these dimensions optimal?
 2. How does actuator mass scale with torque?
 3. Is there a form of walking that is torque minimal? How does it compare to one that is speed maximal? 

## Outline
 1. Kinematic Analysis
    1. Model Definition
        * What is the basic BILL-E model?
    2. Locomotion Primitives Identification
        * What is the bare minimum BILL-E needs to do to locomote on the lattice?  
    3. Locomotion Primitive Optimization 
        * What is the minimum size/complexity robot that accomplishes these locomotion primitives?  
 2. Actuator Selection